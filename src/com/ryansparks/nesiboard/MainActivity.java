package com.ryansparks.nesiboard;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {
	
	Button connect, takePicture;
	ToggleButton pdA, pdB, ledB, ledR, exp3, exp4, exp5;
	TextView console;
	
	private static MainActivity instance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		console = (TextView) findViewById(R.id.console);
		
		connect = (Button) findViewById(R.id.connect);
		takePicture = (Button) findViewById(R.id.takePicture);
		pdA = (ToggleButton) findViewById(R.id.pdAs);
		pdB = (ToggleButton) findViewById(R.id.pdBs);
		ledB = (ToggleButton) findViewById(R.id.ledBs);
		ledR = (ToggleButton) findViewById(R.id.ledRs);
		exp3 = (ToggleButton) findViewById(R.id.exp3s);
		exp4 = (ToggleButton) findViewById(R.id.exp4s);
		exp5 = (ToggleButton) findViewById(R.id.exp5s);
		
		connect.setOnClickListener(this);
		takePicture.setOnClickListener(this);
		pdA.setOnClickListener(this);
		pdB.setOnClickListener(this);
		ledB.setOnClickListener(this);
		ledR.setOnClickListener(this);
		exp3.setOnClickListener(this);
		exp4.setOnClickListener(this);
		exp5.setOnClickListener(this);
		
		instance = this;
		initialize();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initialize() {
		SerialListener s = new SerialListener();
		new Thread(s).start();
		addConsoleMessage("Serial Listening Enabled /TODO/");
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.connect:
			addConsoleMessage("Beginning Bluetooth");
			if(Serial.begin())
				addConsoleMessage("Bluetooth Serial Successful");
			else
				addConsoleMessage("Failed.  This is common, please try again");
			break;
		case R.id.takePicture:
			addConsoleMessage("Sending character \"c\"");
			Serial.sendPacket("c");
			break;
		case R.id.pdAs:
			if(pdA.isChecked()) {
				addConsoleMessage("Sending character \"a\"");
				Serial.sendPacket("a");
			} else {
				addConsoleMessage("Sending character \"b\"");
				Serial.sendPacket("b");
			}
			break;
		case R.id.pdBs:
			if(pdB.isChecked()) {
				addConsoleMessage("Sending character \"d\"");
				Serial.sendPacket("d");
			} else {
				addConsoleMessage("Sending character \"e\"");
				Serial.sendPacket("e");
			}
			break;
		case R.id.ledBs:
			if(ledB.isChecked()) {
				addConsoleMessage("Sending character \"f\"");
				Serial.sendPacket("f");
			} else {
				addConsoleMessage("Sending character \"g\"");
				Serial.sendPacket("g");
			}
			break;
		case R.id.ledRs:
			if(ledR.isChecked()) {
				addConsoleMessage("Sending character \"h\"");
				Serial.sendPacket("h");
			} else {
				addConsoleMessage("Sending character \"i\"");
				Serial.sendPacket("i");
			}
			break;
		case R.id.exp3s:
			if(exp3.isChecked()) {
				addConsoleMessage("Sending character \"j\"");
				Serial.sendPacket("j");
			} else {
				addConsoleMessage("Sending character \"k\"");
				Serial.sendPacket("k");
			}
			break;
		case R.id.exp4s:
			if(exp4.isChecked()) {
				addConsoleMessage("Sending character \"l\"");
				Serial.sendPacket("l");
			} else {
				addConsoleMessage("Sending character \"m\"");
				Serial.sendPacket("m");
			}
			break;
		case R.id.exp5s:
			if(exp5.isChecked()) {
				addConsoleMessage("Sending character \"n\"");
				Serial.sendPacket("n");
			} else {
				addConsoleMessage("Sending character \"o\"");
				Serial.sendPacket("o");
			}
			break;
		}
	}
	
	public void addConsoleMessage(String s) {
		console.setText(s);
	}
	
	public static MainActivity getInstance()
	{
		return instance;
	}
}
