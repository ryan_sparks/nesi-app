package com.ryansparks.nesiboard;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

public class Serial {
	static BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	static BluetoothDevice device;
	static BluetoothSocket socket;
	static OutputStream outputStream;
	static InputStream inputStream;
	
	public static boolean sendPacket(String s)
	{
		try {
			outputStream.write(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}
	
	public static boolean begin()
	{
		if(!adapter.isEnabled())
		{
			Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			MainActivity.getInstance().startActivityForResult(enableBluetooth, 0);
		}
		
		
		Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
		
		if(pairedDevices.size() > 0)
		{
			for(BluetoothDevice device : pairedDevices)
			{
				if(device.getName() == "linvor");
				{
					device = device;
					MainActivity.getInstance().addConsoleMessage("Device " + device.getName() + " connected to Application");
				}
			}
		}
		
		try {
			UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
			socket = device.createRfcommSocketToServiceRecord(uuid);
			socket.connect();
			outputStream = socket.getOutputStream();
			inputStream = socket.getInputStream();
			MainActivity.getInstance().addConsoleMessage("Success!");
		} catch (Exception e) { return false; }
		return true;
	}
	
	public static void closeSerial()
	{
		try {
			socket.close();
			outputStream.close();
			inputStream.close();
		} catch (IOException e) { e.printStackTrace(); }
	}
}
